api = 2
core = 7.x

; Libraries
libraries[holder][directory_name] = holder
libraries[holder][download][type] = file
libraries[holder][download][url] = https://github.com/imsky/holder/archive/v2.4.0.zip
libraries[holder][type] = library
